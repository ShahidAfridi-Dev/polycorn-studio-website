import { useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import Link from 'next/link';
import Image from 'next/image';
import { 
  FaBullhorn, FaCode, FaMobileAlt, FaPaintBrush, 
  FaChevronDown, FaPalette, FaFileInvoiceDollar, FaBookOpen 
} from 'react-icons/fa'; // Import additional icons for the resources submenu

const Header = () => {
    const [isSolutionsOpen, setIsSolutionsOpen] = useState(false);
    const [isResourcesOpen, setIsResourcesOpen] = useState(false); // New state for the Resources submenu

    // Menu items for Solutions with their corresponding icons and text
    const solutionsMenuItems = [
        { icon: FaBullhorn, text: 'SOCIAL MEDIA DESIGN', href: '/social-media', color: 'text-red-500' },
        { icon: FaCode, text: 'WEBSITE DESIGN', href: '/web-design', color: 'text-green-500' },
        { icon: FaMobileAlt, text: 'ILLUSTRATION DESIGN', href: '/illustration', color: 'text-blue-500' },
        { icon: FaPaintBrush, text: 'PRESENTATION DESIGN', href: '/presentation', color: 'text-yellow-500' },
    ];

    // New Menu items for Resources with their corresponding icons and text
    const resourcesMenuItems = [
        { icon: FaPalette, text: 'FREE ILLUSTRATIONS', href: '/free-illustrations', color: 'text-indigo-500' },
        { icon: FaBookOpen, text: 'GUIDES', href: '/guides', color: 'text-pink-500' },
        { icon: FaMobileAlt, text: 'DESIGN BLOG', href: '/design-blog', color: 'text-purple-500' },
        { icon: FaFileInvoiceDollar, text: 'CASE STUDIES', href: '/case-studies', color: 'text-green-500' },
    ];

    // Framer Motion animation configurations
    const variants = {
        open: {
            opacity: 1,
            y: 0,
            transition: {
                staggerChildren: 0.1,
                ease: 'easeInOut',
                duration: 0.2,
            },
        },
        closed: {
            opacity: 0,
            y: -10,
            transition: {
                duration: 0.1,
                ease: 'easeInOut',
            },
        },
    };

    const menuItemVariants = {
        open: {
            opacity: 1,
            y: 0,
        },
        closed: {
            opacity: 0,
            y: -5,
        },
    };

    // Arrow animation variants
    const arrowVariants = {
        open: { rotate: 180, transition: { duration: 0.3, ease: 'easeInOut' } },
        closed: { rotate: 0, transition: { duration: 0.3, ease: 'easeInOut' } },
    };
        // Hover style for the arrow
        const arrowHoverStyle = {
            scale: 1.2, // Slightly scale up on hover
            transition: { duration: 0.2, ease: 'easeInOut' },
        };
    

    return (
        <header className="bg-white shadow-md">
            <div className="container mx-auto flex justify-between items-center p-4">
                {/* Logo */}
                <Link href="/">
                    <span className="flex items-center bg-gray-600">
                        <Image
                            src="/images/logo.png" // Replace with your logo image path
                            alt="Logo"
                            width={200} // Adjust to your logo size
                            height={50} // Adjust to your logo size
                            priority // Preload the image on the initial load
                        />
                    </span>
                </Link>

                {/* Navigation */}
                <nav className="relative">
                    <div className="flex items-center space-x-4">
                        {/* SOLUTIONS menu */}
                        <div className="relative">
                            <button
                                onMouseEnter={() => setIsSolutionsOpen(true)}
                                onMouseLeave={() => setIsSolutionsOpen(false)}
                                className="flex items-center text-gray-600 hover:text-gray-800 font-medium px-4 py-2 rounded-md"
                            >
                                SOLUTIONS
                                <motion.div
                                className="ml-2"
                                    variants={arrowVariants}
                                    animate={isSolutionsOpen ? "open" : "closed"}
                                    whileHover={arrowHoverStyle}
                                >
                                    <FaChevronDown className="ml-2" />
                                </motion.div>
                            </button>
                            <AnimatePresence>
                                {isSolutionsOpen && (
                                    <motion.div
                                        className="absolute left-0 min-w-max bg-white shadow-lg mt-2 rounded-md overflow-hidden z-10"
                                        initial="closed"
                                        animate="open"
                                        exit="closed"
                                        variants={variants}
                                        onMouseEnter={() => setIsSolutionsOpen(true)}
                                        onMouseLeave={() => setIsSolutionsOpen(false)}
                                    >
                                        <div className="grid grid-cols-1 divide-y divide-gray-200">
                                            {solutionsMenuItems.map((item, index) => (
                                                <motion.div
                                                    key={index}
                                                    className="flex items-center gap-4 p-4 hover:bg-gray-100 cursor-pointer"
                                                    variants={menuItemVariants}
                                                    whileHover={{ scale: 1.05 }}
                                                    whileTap={{ scale: 0.95 }}
                                                >
                                                    <item.icon className={`${item.color} text-3xl`} />
                                                    <Link href={item.href}>
                                                        <span className="text-sm font-medium text-gray-700">{item.text}</span>
                                                    </Link>
                                                </motion.div>
                                            ))}
                                        </div>
                                    </motion.div>
                                )}
                            </AnimatePresence>
                        </div>

                        {/* OUR WORK link */}
                        <Link href="/our-work">
                            <span className="text-gray-600 hover:text-gray-800 font-medium px-4 py-2 rounded-md">OUR WORK</span>
                        </Link>

                        {/* PRICING link */}
                        <Link href="/pricing">
                            <span className="text-gray-600 hover:text-gray-800 font-medium px-4 py-2 rounded-md">PRICING</span>
                        </Link>

                        {/* RESOURCES menu */}
                        <div className="relative">
                            <button
                                onMouseEnter={() => setIsResourcesOpen(true)}
                                onMouseLeave={() => setIsResourcesOpen(false)}
                                className="flex items-center text-gray-600 hover:text-gray-800 font-medium px-4 py-2 rounded-md"
                            >
                                RESOURCES
                                <motion.div
                                className="ml-2"
                                    variants={arrowVariants}
                                    animate={isResourcesOpen ? "open" : "closed"}
                                    whileHover={arrowHoverStyle}
                                >
                                    <FaChevronDown className="ml-2" />
                                </motion.div>
                            </button>
                            <AnimatePresence>
                                {isResourcesOpen && (
                                    <motion.div
                                        className="absolute left-0 min-w-max bg-white shadow-lg mt-2 rounded-md overflow-hidden z-10"
                                        initial="closed"
                                        animate="open"
                                        exit="closed"
                                        variants={variants}
                                        onMouseEnter={() => setIsResourcesOpen(true)}
                                        onMouseLeave={() => setIsResourcesOpen(false)}
                                    >
                                        <div className="grid grid-cols-1 divide-y divide-gray-200">
                                            {resourcesMenuItems.map((item, index) => (
                                                <motion.div
                                                    key={index}
                                                    className="flex items-center gap-4 p-4 hover:bg-gray-100 cursor-pointer"
                                                    variants={menuItemVariants}
                                                    whileHover={{ scale: 1.05 }}
                                                    whileTap={{ scale: 0.95 }}
                                                >
                                                    <item.icon className={`${item.color} text-3xl`} />
                                                    <Link href={item.href}>
                                                        <span className="text-sm font-medium text-gray-700">{item.text}</span>
                                                    </Link>
                                                </motion.div>
                                            ))}
                                        </div>
                                    </motion.div>
                                )}
                            </AnimatePresence>
                        </div>
                    </div>
                </nav>

                {/* Action Buttons */}
                <div className="flex items-center space-x-4">
                    <Link href="/signin">
                        <span className="text-sm text-blue-700 hover:text-white hover:bg-blue-500 py-2 px-4 border border-blue-500 rounded-md">
                            SIGN IN
                        </span>
                    </Link>
                    <Link href="/get-started">
                        <span className="text-sm bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded-md">
                            GET STARTED
                        </span>
                    </Link>
                </div>
            </div>
        </header>
    );
};

export default Header;
