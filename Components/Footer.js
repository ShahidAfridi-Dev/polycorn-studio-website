// components/Footer.js
const Footer = () => {
    return (
        <footer className="bg-gray-700 text-white p-4">
            <div className="container mx-auto text-center">
                <p>&copy; {new Date().getFullYear()} My Website</p>
            </div>
        </footer>
    );
};

export default Footer;
