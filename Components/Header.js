import { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import Link from "next/link";
import Image from "next/image";
import {
  FaBullhorn,
  FaCode,
  FaMobileAlt,
  FaPaintBrush,
  FaChevronDown,
  FaPalette,
  FaFileInvoiceDollar,
  FaBookOpen,
  FaBars,
  FaTimes,
} from "react-icons/fa"; // Import additional icons for the resources submenu
import { IoMdClose } from "react-icons/io";
import { IoCloseCircleOutline } from "react-icons/io5";
const Header = () => {
  const [isSolutionsOpen, setIsSolutionsOpen] = useState(false);
  const [isResourcesOpen, setIsResourcesOpen] = useState(false); // New state for the Resources submenu
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const [isMobileSolutionsOpen, setMobileSolutionsOpen] = useState(false);
  const [isMobileResourcesOpen, setMobileResourcesOpen] = useState(false);

  // Menu items for Solutions with their corresponding icons and text
  const solutionsMenuItems = [
    {
      icon: FaBullhorn,
      text: "SOCIAL MEDIA DESIGN",
      href: "/social-media",
      color: "text-red-500",
    },
    {
      icon: FaCode,
      text: "WEBSITE DESIGN",
      href: "/web-design",
      color: "text-green-500",
    },
    {
      icon: FaMobileAlt,
      text: "ILLUSTRATION DESIGN",
      href: "/illustration",
      color: "text-blue-500",
    },
    {
      icon: FaPaintBrush,
      text: "PRESENTATION DESIGN",
      href: "/presentation",
      color: "text-yellow-500",
    },
  ];

  // New Menu items for Resources with their corresponding icons and text
  const resourcesMenuItems = [
    {
      icon: FaPalette,
      text: "FREE ILLUSTRATIONS",
      href: "/free-illustrations",
      color: "text-indigo-500",
    },
    {
      icon: FaBookOpen,
      text: "GUIDES",
      href: "/guides",
      color: "text-pink-500",
    },
    {
      icon: FaMobileAlt,
      text: "DESIGN BLOG",
      href: "/design-blog",
      color: "text-purple-500",
    },
    {
      icon: FaFileInvoiceDollar,
      text: "CASE STUDIES",
      href: "/case-studies",
      color: "text-green-500",
    },
  ];

  // Framer Motion animation configurations
  const variants = {
    open: {
      opacity: 1,
      y: 0,
      transition: {
        staggerChildren: 0.1,
        ease: "easeInOut",
        duration: 0.5,
      },
    },
    closed: {
      opacity: 0,
      y: -10,
      transition: {
        duration: 0.5,
        ease: "easeInOut",
      },
    },
  };

  const menuItemVariants = {
    open: {
      opacity: 1,
      y: 0,
    },
    closed: {
      opacity: 0,
      y: -5,
    },
  };

  // Arrow animation variants
  const arrowVariants = {
    open: { rotate: 180, transition: { duration: 0.3, ease: "easeInOut" } },
    closed: { rotate: 0, transition: { duration: 0.3, ease: "easeInOut" } },
  };
  // Hover style for the arrow
  const arrowHoverStyle = {
    scale: 1.2, // Slightly scale up on hover
    transition: { duration: 0.2, ease: "easeInOut" },
  };

  const mobileMenuVariants = {
    open: { opacity: 1, x: 0 },
    closed: { opacity: 0, x: "-100%" },
  };
  // Function to close the mobile menu
  const closeMobileMenu = () => setIsMobileMenuOpen(false);
  return (
    <header className="bg-white shadow-md">
      <div className="container mx-auto flex items-center justify-between p-4">
        {/* Logo */}
        <Link href="/">
          <span className="flex items-center justify-center sm:justify-start bg-gray-600">
            <Image
              src="/images/logo.png" // Replace with your logo image path
              alt="Logo"
              width={200} // Adjust to your logo size
              height={50} // Adjust to your logo size
              priority // Preload the image on the initial load
            />
          </span>
        </Link>
        {/* Hamburger Icon */}
        <div className="flex md:hidden">
          <button onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)}>
            <FaBars className="text-2xl" />
          </button>
        </div>

        {/* Navigation */}
        <nav className="hidden lg:flex flex-col  items-center mt-4 sm:mt-0">
          <div className="flex items-center space-x-4">
            {/* SOLUTIONS menu */}
            <div className="relative text-sm xl:text-lg">
              <button
                onMouseEnter={() => setIsSolutionsOpen(true)}
                onMouseLeave={() => setIsSolutionsOpen(false)}
                className="flex items-center text-gray-600 hover:text-gray-800 font-medium px-4 py-2 rounded-md"
              >
                SOLUTIONS
                <motion.div
                  className="ml-2"
                  variants={arrowVariants}
                  animate={isSolutionsOpen ? "open" : "closed"}
                  whileHover={arrowHoverStyle}
                >
                  <FaChevronDown className="ml-2" />
                </motion.div>
              </button>
              <AnimatePresence>
                {isSolutionsOpen && (
                  <motion.div
                    className="absolute left-0 min-w-max bg-white shadow-lg mt-2 rounded-md overflow-hidden z-10"
                    initial="closed"
                    animate="open"
                    exit="closed"
                    variants={variants}
                    onMouseEnter={() => setIsSolutionsOpen(true)}
                    onMouseLeave={() => setIsSolutionsOpen(false)}
                  >
                    <div className="grid grid-cols-1 divide-y divide-gray-200">
                      {solutionsMenuItems.map((item, index) => (
                        <motion.div
                          key={index}
                          className="flex items-center gap-4 p-4 hover:bg-gray-100 cursor-pointer"
                          variants={menuItemVariants}
                          whileHover={{ scale: 1.05 }}
                          whileTap={{ scale: 0.95 }}
                        >
                          <item.icon className={`${item.color} text-3xl`} />
                          <Link href={item.href}>
                            <span className="text-sm font-medium text-gray-700">
                              {item.text}
                            </span>
                          </Link>
                        </motion.div>
                      ))}
                    </div>
                  </motion.div>
                )}
              </AnimatePresence>
            </div>

            {/* OUR WORK link */}
            <Link href="/our-work">
              <span className="text-gray-600 text-sm xl:text-lg hover:text-gray-800 font-medium px-4 py-2 rounded-md">
                OUR WORK
              </span>
            </Link>

            {/* PRICING link */}
            <Link href="/pricing">
              <span className="text-gray-600 text-sm xl:text-lg hover:text-gray-800 font-medium px-4 py-2 rounded-md">
                PRICING
              </span>
            </Link>

            {/* RESOURCES menu */}
            <div className="relative text-sm xl:text-lg">
              <button
                onMouseEnter={() => setIsResourcesOpen(true)}
                onMouseLeave={() => setIsResourcesOpen(false)}
                className="flex items-center text-gray-600 hover:text-gray-800 font-medium px-4 py-2 rounded-md"
              >
                RESOURCES
                <motion.div
                  className="ml-2"
                  variants={arrowVariants}
                  animate={isResourcesOpen ? "open" : "closed"}
                  whileHover={arrowHoverStyle}
                >
                  <FaChevronDown className="ml-2" />
                </motion.div>
              </button>
              <AnimatePresence>
                {isResourcesOpen && (
                  <motion.div
                    className="absolute left-0 min-w-max bg-white shadow-lg mt-2 rounded-md overflow-hidden z-10"
                    initial="closed"
                    animate="open"
                    exit="closed"
                    variants={variants}
                    onMouseEnter={() => setIsResourcesOpen(true)}
                    onMouseLeave={() => setIsResourcesOpen(false)}
                  >
                    <div className="grid grid-cols-1 divide-y divide-gray-200">
                      {resourcesMenuItems.map((item, index) => (
                        <motion.div
                          key={index}
                          className="flex items-center gap-4 p-4 hover:bg-gray-100 cursor-pointer"
                          variants={menuItemVariants}
                          whileHover={{ scale: 1.05 }}
                          whileTap={{ scale: 0.95 }}
                        >
                          <item.icon className={`${item.color} text-3xl`} />
                          <Link href={item.href}>
                            <span className="text-sm font-medium text-gray-700">
                              {item.text}
                            </span>
                          </Link>
                        </motion.div>
                      ))}
                    </div>
                  </motion.div>
                )}
              </AnimatePresence>
            </div>
          </div>
        </nav>
        {/* Mobile Menu */}
        <AnimatePresence>
          {isMobileMenuOpen && (
            <motion.div
              className="fixed inset-0 z-10 bg-white p-8 overflow-auto"
              initial={{ x: "100%" }}
              animate={{ x: 0 }}
              exit={{ x: "100%" }}
              transition={{ type: "spring", stiffness: 75 }}
            >
              {/* Close icon */}
              {/* Close icon */}
              <button
                onClick={closeMobileMenu}
                className="absolute top-3 right-4"
                whileHover={{ scale: 1.1 }}
                whileTap={{ scale: 0.9 }}
                style={{
                  background: "linear-gradient(135deg, #6D5BBA, #8D58BF)",
                  // boxShadow: '0px 2px 15px rgba(109, 91, 186, 0.5)',
                  borderRadius: "50%",
                  padding: "0.5rem",
                }}
              >
                <motion.div
                  className="text-lg"
                  style={{ color: "#FFFFFF" }}
                  initial={{ scale: 1 }}
                  animate={{ scale: 1 }}
                  whileHover={{ scale: 1.1, rotate: 90 }}
                  whileTap={{ scale: 0.9, rotate: 180 }}
                  transition={{ type: "spring", stiffness: 300 }}
                >
                  <FaTimes />
                </motion.div>
              </button>

              {/* Mobile navigation links */}
              {/* SOLUTIONS Dropdown */}
              <div className="p-4 mt-4 border-b">
                <button
                  onClick={() => setMobileSolutionsOpen(!isMobileSolutionsOpen)}
                  className="flex items-center justify-between w-full text-gray-600 font-medium"
                >
                  SOLUTIONS
                  <FaChevronDown
                    className={`transition-transform ${
                      isMobileSolutionsOpen ? "rotate-180" : ""
                    }`}
                  />
                </button>
                {isMobileSolutionsOpen && (
                  <motion.div
                    className="mt-4"
                    initial="closed"
                    animate="open"
                    exit="closed"
                    variants={variants}
                  >
                    {solutionsMenuItems.map((item, index) => (
                      <Link
                        key={index}
                        href={item.href}
                        onClick={closeMobileMenu}
                      >
                        <span className="flex items-center gap-4 p-4 hover:bg-gray-100">
                          <item.icon className={`${item.color} text-3xl`} />
                          <span className="text-sm font-medium text-gray-700">
                            {item.text}
                          </span>
                        </span>
                      </Link>
                    ))}
                  </motion.div>
                )}
              </div>
              {/* Other Navigation Links */}
              <Link href="/our-work">
                <span
                  className="flex items-center p-4 border-b text-gray-600 hover:bg-gray-100 font-medium"
                  onClick={closeMobileMenu}
                >
                  OUR WORK
                </span>
              </Link>
              <Link href="/pricing">
                <span
                  className="flex items-center p-4 border-b text-gray-600 hover:bg-gray-100 font-medium"
                  onClick={closeMobileMenu}
                >
                  PRICING
                </span>
              </Link>
              {/* RESOURCES Dropdown */}
              <div className="p-4">
                <button
                  onClick={() => setMobileResourcesOpen(!isMobileResourcesOpen)}
                  className="flex items-center justify-between w-full text-gray-600 font-medium"
                >
                  RESOURCES
                  <FaChevronDown
                    className={`transition-transform ${
                      isMobileResourcesOpen ? "rotate-180" : ""
                    }`}
                  />
                </button>
                {isMobileResourcesOpen && (
                  <motion.div
                    className="mt-4"
                    initial="closed"
                    animate="open"
                    exit="closed"
                    variants={variants}
                  >
                    {resourcesMenuItems.map((item, index) => (
                      <Link
                        key={index}
                        href={item.href}
                        onClick={closeMobileMenu}
                      >
                        <span className="flex items-center gap-4 p-4 hover:bg-gray-100">
                          <item.icon className={`${item.color} text-3xl`} />
                          <span className="text-sm font-medium text-gray-700">
                            {item.text}
                          </span>
                        </span>
                      </Link>
                    ))}
                  </motion.div>
                )}
              </div>
              <div className="mt-4 sm:hidden flex gap-4">
              <Link href="/signin">
        <span className="text-sm py-2 px-4 rounded-md transition-all duration-300 ease-in-out border-2 border-transparent hover:border-indigo-500 bg-gradient-to-r from-indigo-600 to-indigo-700 hover:from-indigo-700 hover:to-indigo-800 shadow-md hover:shadow-lg text-white cursor-pointer">
            SIGN IN
        </span>
    </Link>
    <Link href="/get-started">
        <span className="text-sm py-2 px-4 rounded-md transition-all duration-300 ease-in-out bg-gradient-to-r from-indigo-500 to-indigo-600 hover:from-indigo-600 hover:to-indigo-700 shadow-md hover:shadow-lg text-white cursor-pointer">
            GET STARTED
        </span>
    </Link>
              </div>
            </motion.div>
          )}
        </AnimatePresence>
        {/* Action Buttons */}
      
        <div className="hidden md:flex flex-col items-center mt-4 space-y-2 md:flex-row md:space-y-0 md:space-x-4 md:mt-0">
    <Link href="/signin">
        <span className="text-sm py-2 px-4 rounded-md transition-all duration-300 ease-in-out border-2 border-transparent hover:border-indigo-500 bg-gradient-to-r from-indigo-600 to-indigo-700 hover:from-indigo-700 hover:to-indigo-800 shadow-md hover:shadow-lg text-white cursor-pointer">
            SIGN IN
        </span>
    </Link>
    <Link href="/get-started">
        <span className="text-sm py-2 px-4 rounded-md transition-all duration-300 ease-in-out bg-gradient-to-r from-indigo-500 to-indigo-600 hover:from-indigo-600 hover:to-indigo-700 shadow-md hover:shadow-lg text-white cursor-pointer">
            GET STARTED
        </span>
    </Link>
    <div className="flex lg:hidden">
    <button onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)} className="lg:hidden">
        <FaBars className="text-2xl" />
    </button>
        </div>
</div>

      </div>
    </header>
  );
};

export default Header;
