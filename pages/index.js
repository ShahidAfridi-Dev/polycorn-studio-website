// pages/index.js
import Layout from '../Components/Layout';

const Home = () => {
    return (
        <Layout>
            <div className="container mx-auto p-4">
                <h2 className="text-2xl font-bold">Welcome to My Website</h2>
                <p>This is the home page content.</p>
            </div>
        </Layout>
    );
};

export default Home;
